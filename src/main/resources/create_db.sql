DROP TABLE IF EXISTS commandes;
DROP TABLE IF EXISTS produit;

CREATE TABLE commandes
(
    id          int AUTO_INCREMENT NOT NULL
    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE produit
(
    id          int AUTO_INCREMENT  NOT NULL,
    nom         VARCHAR(255),
    quantite    int                 NOT NULL,
    prix        int                 NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB;
