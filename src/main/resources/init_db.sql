INSERT INTO produit (id, nom, quantite, prix) VALUES (1, 'Chocapic', '10', '2');
INSERT INTO produit (id, nom, quantite, prix) VALUES (2, 'Pain', '10', '1');
INSERT INTO produit (id, nom, quantite, prix) VALUES (3, 'Chocolat', '10', '3');
INSERT INTO produit (id, nom, quantite, prix) VALUES (4, 'Lessive', '10', '4');

INSERT INTO commandes (id) VALUES (1);
INSERT INTO commandes (id) VALUES (2);
INSERT INTO commandes (id) VALUES (3);

INSERT INTO produit_orders (products_id, orders_id) VALUES ('1', '1');
INSERT INTO produit_orders (products_id, orders_id) VALUES ('2', '1');
INSERT INTO produit_orders (products_id, orders_id) VALUES ('3', '1');

INSERT INTO produit_orders (products_id, orders_id) VALUES ('1', '2');
INSERT INTO produit_orders (products_id, orders_id) VALUES ('2', '2');

INSERT INTO produit_orders (products_id, orders_id) VALUES ('4', '3');

SELECT setval('produit_id_seq', 4, true); -- next value will be 5
SELECT setval('commandes_id_seq', 3, true); -- next value will be 4

