package com.stock.stock.api;

import com.stock.stock.dto.ProductDto;
import com.stock.stock.mapper.ProductMapper;
import com.stock.stock.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {


    private final ProductService productService;
    private final ProductMapper productMapper;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of products ")
    public ResponseEntity<List<ProductDto>> getAll() {

        return ResponseEntity.ok(
                this.productService.getAllProducts().stream()
                        .map(this.productMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a product by id")
    public ResponseEntity<ProductDto> getById(@PathVariable final Long id) {
        return ResponseEntity.ok(
                this.productMapper.mapToDto(this.productService.getProductById(id))
        );
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "create a product")
    public ResponseEntity<ProductDto> createProduct(@RequestBody final ProductDto productDto) {
        ProductDto productDtoResponse =
                this.productMapper.mapToDto(
                        this.productService.createProduct(
                                this.productMapper.mapToModel(productDto)
                        ));

        return ResponseEntity
                .created(URI.create("/v1/products/" + productDtoResponse.getId()))
                .body(productDtoResponse);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "remove a proudct by id")
    public void deleteById(@PathVariable final Long id) {
                this.productService.deleteProductById(id);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary = "Update a product")
    public ResponseEntity<Void> updateProduct(@PathVariable final Long id, @RequestBody ProductDto productDto) {
            productDto.setId(id);
            this.productService.updateProduct(productMapper.mapToModel(productDto));
            return ResponseEntity.noContent().build();
    }


}
