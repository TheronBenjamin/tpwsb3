package com.stock.stock.api;

import com.stock.stock.mapper.OrderMapper;
import com.stock.stock.dto.OrderDto;
import com.stock.stock.model.Order;
import com.stock.stock.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final OrderMapper orderMapper;


    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of Orders ")
    public ResponseEntity<List<OrderDto>> getAll() {

        return ResponseEntity.ok(
                this.orderService.getAllOrders()
                        .stream()
                        .map(this.orderMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return an order by id")
    public ResponseEntity<OrderDto> getById(@PathVariable final Long id) {
        return ResponseEntity.ok(
                this.orderMapper.mapToDto(this.orderService.getOrderById(id))
        );
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "create a product")
    public ResponseEntity<OrderDto> createProduct(@RequestBody final OrderDto orderDto) {
        Order order = this.orderMapper.mapToModel(orderDto);
        List<Long> productList = new ArrayList<>();
        //populate
        orderDto.getProducts().forEach(product ->
                productList.add(product.getId())
        );
        Order orderPopulate = this.orderService.populateProductsToOrder(order,productList);
        //set quantity
        orderPopulate.getProducts().forEach(product -> {
            product.setQuantite(product.getQuantite() - 1);
        });
        //create
        Order createdOrder = this.orderService.createOrder(orderPopulate);
        OrderDto orderDtoResponse =
                this.orderMapper.mapToDto(createdOrder);
        return ResponseEntity
                .created(URI.create("/v1/orders/" + orderDtoResponse.getId()))
                .body(orderDtoResponse);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "remove an order by id")
    public void deleteById(@PathVariable final Long id) {
        this.orderService.deleteOrder(id);
    }

}
