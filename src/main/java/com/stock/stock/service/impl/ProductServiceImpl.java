package com.stock.stock.service.impl;

import com.stock.stock.repository.ProductRepository;
import com.stock.stock.service.ProductService;
import com.stock.stock.model.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProductById(Long id) {
        return productRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Product not found")
        );
    }

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product updateProduct(Product product) {
        Product productToUpdate = getProductById(product.getId());
        productToUpdate.setPrix(product.getPrix());
        productToUpdate.setNom(product.getNom());
        productToUpdate.setQuantite(product.getQuantite());
        productToUpdate.setOrders(product.getOrders());
        return productRepository.save(productToUpdate);
    }
}
