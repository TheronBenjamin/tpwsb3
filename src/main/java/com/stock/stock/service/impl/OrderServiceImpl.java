package com.stock.stock.service.impl;

import com.stock.stock.model.Order;
import com.stock.stock.model.Product;
import com.stock.stock.repository.OrderRepository;
import com.stock.stock.service.OrderService;
import com.stock.stock.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ProductService productService;

    @Override
    public List<Order> getAllOrders() {
        return this.orderRepository.findAll();
    }

    @Override
    public Order getOrderById(Long id) {
        return this.orderRepository.getReferenceById(id);
    }

    @Override
    public Order createOrder(Order order) {
        List<Product> productList = new ArrayList<>();
        order.getProducts().forEach(product -> {
            System.out.println("blv"+product.getId());
            Product productToAdd = productService.getProductById(product.getId());
            productList.add(productToAdd);
        });
        order.setProducts(productList);
        return orderRepository.save(order);
    }

    @Override
    public void deleteOrder(Long id) {
        Order orderToDelete = this.getOrderById(id);
        this.orderRepository.delete(orderToDelete);
    }

    @Override
    public Order populateProductsToOrder(Order order, List<Long> productIds) {
        productIds.forEach(productId -> {
            Product product = this.productService.getProductById(productId);
            order.getProducts().add(product);
        });
        return order;
    }
}
