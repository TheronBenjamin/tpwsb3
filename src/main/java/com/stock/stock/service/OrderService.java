package com.stock.stock.service;

import com.stock.stock.model.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAllOrders();

    Order populateProductsToOrder(Order order, List<Long> productIds);

    Order getOrderById(Long id);

    Order createOrder(Order order);

    void deleteOrder(Long id);
}

