package com.stock.stock.service;

import com.stock.stock.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();

    Product getProductById(Long id);

    Product createProduct(Product product);

    void deleteProductById(Long id);

    Product updateProduct(Product product);

}
