package com.stock.stock.mapper;

import com.stock.stock.dto.OrderDto;
import com.stock.stock.dto.ProductDto;
import com.stock.stock.model.Order;
import com.stock.stock.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderDto mapToDto(Order order);

    @Mapping(target = "products", ignore = true)
    Order mapToModel(OrderDto orderDto);

    default List<Product> getProductList(OrderDto orderDto) {

        List<Product> productList = new ArrayList<>();
        productList = orderDto.getProducts().stream().map(product -> new Product(
                product.getId(),
                product.getNom(),
                product.getQuantite(),
                product.getPrix(),
                null
        )).toList();
        return productList;
    }
}
