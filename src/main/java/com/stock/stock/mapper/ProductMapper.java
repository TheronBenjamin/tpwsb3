package com.stock.stock.mapper;

import com.stock.stock.dto.ProductDto;
import com.stock.stock.model.Order;
import com.stock.stock.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto mapToDto(Product product);

    @Mapping(target = "orders", ignore = true)
    Product mapToModel(ProductDto productDto);


    default List<Long> getOrdersIds(List<Order> orderList) {
        return orderList.stream()
                .map(Order::getId)
                .toList();
    }



}
