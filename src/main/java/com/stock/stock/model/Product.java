package com.stock.stock.model;

import javax.persistence.*;

import lombok.*;
import org.apache.tomcat.util.digester.ArrayStack;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "produit")
public class Product {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(
            nullable = false,
            name = "nom",
            length = 100
    )
    private String nom;

    @Column(
            nullable = false,
            name = "quantite"
    )
    private int quantite;

    @Column(
            nullable = false,
            name = "prix"
    )
    private long prix;

    @ManyToMany(
            fetch = FetchType.LAZY
    )
    private List<Order> orders = new ArrayList<>();

}
